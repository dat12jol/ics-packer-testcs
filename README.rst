Packer template to build ESS DM
===============================

This Packer_ template creates the ESS DM Vagrant_ box.
The repository also contains all the scripts needed to create a bootable USB image
to install the Development machine.

Usage
-----

Images are built automatically by Jenkins (you can check the Jenkinsfile).

To test locally you can run the following commands:

1. To be able to push the box to Vagrant cloud, you first have to set the VAGRANTCLOUD_TOKEN environment variable::

    $ export VAGRANTCLOUD_TOKEN=xxxxxxxx

2. To build the vagrant box, run::

    $ export DEVENV_VERSION=$(git describe)
    $ packer build devenv-vm.json

   This will create and push the box `esss/devenv-7.3`.

3. To build the USB bootable image, run::

    $ cd physical
    $ ./build_img.sh

   This will create the `physical/build/devenv_ks.<DEVENV_VERSION>.img` image.


.. _Packer: https://www.packer.io
.. _Vagrant: https://www.vagrantup.com
